﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace ACWorldSpawnManipulator.JsonClasses.LandBlocks
{
    public class WeenieValues
    {
        [JsonProperty("links")]
        public List<GroupLink> Links { get; set; }

        [JsonProperty("weenies")]
        public List<Weenie> Weenies { get; set; }

        public long GetNextWeenieID()
        {
            List<long> weenieID = new List<long>();

            foreach (Weenie w in Weenies)
                weenieID.Add(w.ID);

            return weenieID.Last() + 1;
        }
        public Weenie GetWeenieByID(long id)
        {
            Weenie result = null;
            foreach (Weenie w in Weenies)
            {
                if (w.ID == id)
                {
                    result = w;
                    break;
                }
            }
            return result;
        }
        public void RemoveWeenieByID(long id)
        {
            int index = -1;

            for (int i = 0; i < Weenies.Count; i++)
            {
                if (Weenies[i].ID == id)
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
            {
                try
                {
                    Weenies.RemoveAt(index);
                }
                catch { }
            }
        }
    }
}
