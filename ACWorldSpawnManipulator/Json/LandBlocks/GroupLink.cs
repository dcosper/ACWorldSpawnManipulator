﻿using Newtonsoft.Json;

namespace ACWorldSpawnManipulator.JsonClasses.LandBlocks
{
    public class GroupLink
    {
        [JsonProperty("source")]
        public int Source { get; set; }

        [JsonProperty("target")]
        public int Target { get; set; }
    }
}
