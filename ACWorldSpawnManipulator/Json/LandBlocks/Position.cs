﻿using Newtonsoft.Json;

namespace ACWorldSpawnManipulator.JsonClasses.LandBlocks
{
    public class Position
    {
        [JsonProperty("frame")]
        public PositionFrame Frame { get; set; }

        [JsonProperty("objcell_id")]
        public long ObjCellID { get; set; }

        public Position() { }

        public Position(float x, float y, float z, float aw, float ax, float ay, float az, long objCellID)
        {
            ObjCellID = objCellID;
            Frame = new PositionFrame(x, y, z, aw, ax, ay, az);
        }
    }
}
