﻿using Newtonsoft.Json;

namespace ACWorldSpawnManipulator.JsonClasses.LandBlocks
{
    public class Weenie
    {
        [JsonProperty("id")]
        public long ID { get; set; }

        [JsonProperty("pos")]
        public Position Position { get; set; }

        [JsonProperty("wcid")]
        public long WCID { get; set; }

        public Weenie() { }

        public Weenie(long id, Position position, long wcid)
        {
            ID = id;
            Position = position;
            WCID = wcid;
        }
        public override string ToString()
        {
            return ID.ToString();
        }
    }
}
