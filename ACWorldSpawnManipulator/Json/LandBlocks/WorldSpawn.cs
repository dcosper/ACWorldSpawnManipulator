﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ACWorldSpawnManipulator.JsonClasses.LandBlocks
{
    public class WorldSpawn
    {
        [JsonProperty("landblocks")]
        public List<LandBlockSpawn> LandBlockSpawns { get; set; }
        public WorldSpawn()
        {
            LandBlockSpawns = new List<LandBlockSpawn>();
        }
        public LandBlockSpawn GetLandBlockByKey(long key)
        {
            LandBlockSpawn result = null;
            try
            {
                foreach (LandBlockSpawn lbs in LandBlockSpawns)
                {
                    if (lbs.Key == key)
                    {
                        result = lbs;
                        break;
                    }
                }
            }
            catch { }
            return result;
        }
        public void RemoveLandBlockByKey(long key)
        {
            int index = -1;

            for (int i = 0; i < LandBlockSpawns.Count; i++)
            {
                if (LandBlockSpawns[i].Key == key)
                {
                    index = i;
                    break;
                }
            }
            if (index != -1)
            {
                LandBlockSpawns.RemoveAt(index);
            }
        }
    }
}
