﻿using Newtonsoft.Json;

namespace ACWorldSpawnManipulator.JsonClasses.LandBlocks
{
    public class LandBlockSpawn
    {
        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("value")]
        public WeenieValues Value { get; set; }

        public LandBlockSpawn()
        {
            Key = -1;
        }
        public LandBlockSpawn(LandBlockSpawn copy)
        {
            Key = copy.Key;
            Value = copy.Value;
        }
        public override string ToString()
        {
            return Key.ToString();
        }
    }
}
