﻿using Newtonsoft.Json;

namespace ACWorldSpawnManipulator.JsonClasses.LandBlocks
{
    public class PositionFrame
    {
        [JsonProperty("angles")]
        public Angle Angle { get; set; }

        [JsonProperty("origin")]
        public Origin Origin { get; set; }

        public PositionFrame() { }

        public PositionFrame(float x, float y, float z, float aw, float ax, float ay, float az)
        {
            Angle = new Angle(aw, ax, ay, az);
            Origin = new Origin(x, y, z);
        }
    }
}
