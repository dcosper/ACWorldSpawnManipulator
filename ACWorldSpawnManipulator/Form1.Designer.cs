﻿namespace ACACWorldSpawnManipulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuFolder = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reorderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuWeenie = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.renumberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtfOutput = new System.Windows.Forms.RichTextBox();
            this.button_AddNew = new System.Windows.Forms.Button();
            this.textBox_WeenieID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button_DeleteWeenie = new System.Windows.Forms.Button();
            this.button_ClearWeenie = new System.Windows.Forms.Button();
            this.button_UpdateWeenie = new System.Windows.Forms.Button();
            this.button_ParseLocClear = new System.Windows.Forms.Button();
            this.button_ParseLoc = new System.Windows.Forms.Button();
            this.textBox_ParseLoc = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_WCID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_CellID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_AngleZ = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_AngleY = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_PosZ = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_AngleX = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_PosY = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_AngleW = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_PosX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.contextMenuFolder.SuspendLayout();
            this.contextMenuWeenie.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Right;
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(363, 28);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(437, 422);
            this.treeView1.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Folder-Blank-icon.png");
            this.imageList1.Images.SetKeyName(1, "Folder-Private-icon.png");
            this.imageList1.Images.SetKeyName(2, "icon_07.png");
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(129, 26);
            this.importToolStripMenuItem.Text = "&Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(129, 26);
            this.exportToolStripMenuItem.Text = "&Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // contextMenuFolder
            // 
            this.contextMenuFolder.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuFolder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem1,
            this.reorderToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.contextMenuFolder.Name = "contextMenuFolder";
            this.contextMenuFolder.Size = new System.Drawing.Size(211, 104);
            // 
            // exportToolStripMenuItem1
            // 
            this.exportToolStripMenuItem1.Name = "exportToolStripMenuItem1";
            this.exportToolStripMenuItem1.Size = new System.Drawing.Size(210, 24);
            this.exportToolStripMenuItem1.Text = "Export";
            this.exportToolStripMenuItem1.Click += new System.EventHandler(this.exportToolStripMenuItem1_Click);
            // 
            // reorderToolStripMenuItem
            // 
            this.reorderToolStripMenuItem.Name = "reorderToolStripMenuItem";
            this.reorderToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.reorderToolStripMenuItem.Text = "Reorder";
            this.reorderToolStripMenuItem.Click += new System.EventHandler(this.reorderToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // contextMenuWeenie
            // 
            this.contextMenuWeenie.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuWeenie.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renumberToolStripMenuItem,
            this.deleteToolStripMenuItem1});
            this.contextMenuWeenie.Name = "contextMenuWeenie";
            this.contextMenuWeenie.Size = new System.Drawing.Size(147, 52);
            // 
            // renumberToolStripMenuItem
            // 
            this.renumberToolStripMenuItem.Name = "renumberToolStripMenuItem";
            this.renumberToolStripMenuItem.Size = new System.Drawing.Size(146, 24);
            this.renumberToolStripMenuItem.Text = "Renumber";
            this.renumberToolStripMenuItem.Click += new System.EventHandler(this.renumberToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(146, 24);
            this.deleteToolStripMenuItem1.Text = "Delete";
            this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rtfOutput);
            this.groupBox1.Controls.Add(this.button_AddNew);
            this.groupBox1.Controls.Add(this.textBox_WeenieID);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.button_DeleteWeenie);
            this.groupBox1.Controls.Add(this.button_ClearWeenie);
            this.groupBox1.Controls.Add(this.button_UpdateWeenie);
            this.groupBox1.Controls.Add(this.button_ParseLocClear);
            this.groupBox1.Controls.Add(this.button_ParseLoc);
            this.groupBox1.Controls.Add(this.textBox_ParseLoc);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBox_WCID);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBox_CellID);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBox_AngleZ);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox_AngleY);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox_PosZ);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox_AngleX);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox_PosY);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox_AngleW);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox_PosX);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(357, 422);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Weenie";
            // 
            // rtfOutput
            // 
            this.rtfOutput.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtfOutput.Location = new System.Drawing.Point(3, 338);
            this.rtfOutput.Name = "rtfOutput";
            this.rtfOutput.ReadOnly = true;
            this.rtfOutput.Size = new System.Drawing.Size(351, 81);
            this.rtfOutput.TabIndex = 30;
            this.rtfOutput.Text = "";
            // 
            // button_AddNew
            // 
            this.button_AddNew.Location = new System.Drawing.Point(267, 306);
            this.button_AddNew.Name = "button_AddNew";
            this.button_AddNew.Size = new System.Drawing.Size(75, 23);
            this.button_AddNew.TabIndex = 29;
            this.button_AddNew.Text = "Add New";
            this.button_AddNew.UseVisualStyleBackColor = true;
            // 
            // textBox_WeenieID
            // 
            this.textBox_WeenieID.Location = new System.Drawing.Point(85, 213);
            this.textBox_WeenieID.Name = "textBox_WeenieID";
            this.textBox_WeenieID.Size = new System.Drawing.Size(100, 22);
            this.textBox_WeenieID.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 216);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 17);
            this.label12.TabIndex = 27;
            this.label12.Text = "WeenieID:";
            // 
            // button_DeleteWeenie
            // 
            this.button_DeleteWeenie.Location = new System.Drawing.Point(181, 306);
            this.button_DeleteWeenie.Name = "button_DeleteWeenie";
            this.button_DeleteWeenie.Size = new System.Drawing.Size(75, 23);
            this.button_DeleteWeenie.TabIndex = 26;
            this.button_DeleteWeenie.Text = "Delete";
            this.button_DeleteWeenie.UseVisualStyleBackColor = true;
            this.button_DeleteWeenie.Click += new System.EventHandler(this.button_DeleteWeenie_Click);
            // 
            // button_ClearWeenie
            // 
            this.button_ClearWeenie.Location = new System.Drawing.Point(97, 306);
            this.button_ClearWeenie.Name = "button_ClearWeenie";
            this.button_ClearWeenie.Size = new System.Drawing.Size(75, 23);
            this.button_ClearWeenie.TabIndex = 25;
            this.button_ClearWeenie.Text = "Clear";
            this.button_ClearWeenie.UseVisualStyleBackColor = true;
            this.button_ClearWeenie.Click += new System.EventHandler(this.button_ClearWeenie_Click);
            // 
            // button_UpdateWeenie
            // 
            this.button_UpdateWeenie.Location = new System.Drawing.Point(7, 306);
            this.button_UpdateWeenie.Name = "button_UpdateWeenie";
            this.button_UpdateWeenie.Size = new System.Drawing.Size(75, 23);
            this.button_UpdateWeenie.TabIndex = 24;
            this.button_UpdateWeenie.Text = "Update";
            this.button_UpdateWeenie.UseVisualStyleBackColor = true;
            this.button_UpdateWeenie.Click += new System.EventHandler(this.button_UpdateWeenie_Click);
            // 
            // button_ParseLocClear
            // 
            this.button_ParseLocClear.Location = new System.Drawing.Point(209, 84);
            this.button_ParseLocClear.Name = "button_ParseLocClear";
            this.button_ParseLocClear.Size = new System.Drawing.Size(75, 23);
            this.button_ParseLocClear.TabIndex = 23;
            this.button_ParseLocClear.Text = "Clear";
            this.button_ParseLocClear.UseVisualStyleBackColor = true;
            this.button_ParseLocClear.Click += new System.EventHandler(this.button_ParseLocClear_Click);
            // 
            // button_ParseLoc
            // 
            this.button_ParseLoc.Location = new System.Drawing.Point(128, 84);
            this.button_ParseLoc.Name = "button_ParseLoc";
            this.button_ParseLoc.Size = new System.Drawing.Size(75, 23);
            this.button_ParseLoc.TabIndex = 22;
            this.button_ParseLoc.Text = "Parse";
            this.button_ParseLoc.UseVisualStyleBackColor = true;
            this.button_ParseLoc.Click += new System.EventHandler(this.button_ParseLoc_Click);
            // 
            // textBox_ParseLoc
            // 
            this.textBox_ParseLoc.Location = new System.Drawing.Point(85, 56);
            this.textBox_ParseLoc.Name = "textBox_ParseLoc";
            this.textBox_ParseLoc.Size = new System.Drawing.Size(264, 22);
            this.textBox_ParseLoc.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 17);
            this.label11.TabIndex = 20;
            this.label11.Text = "ParseLoc:";
            // 
            // textBox_WCID
            // 
            this.textBox_WCID.Location = new System.Drawing.Point(242, 257);
            this.textBox_WCID.Name = "textBox_WCID";
            this.textBox_WCID.Size = new System.Drawing.Size(100, 22);
            this.textBox_WCID.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(178, 260);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 17);
            this.label9.TabIndex = 17;
            this.label9.Text = "WCID:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(363, 167);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 17);
            this.label10.TabIndex = 16;
            this.label10.Text = "Angle Z:";
            // 
            // textBox_CellID
            // 
            this.textBox_CellID.Location = new System.Drawing.Point(72, 257);
            this.textBox_CellID.Name = "textBox_CellID";
            this.textBox_CellID.Size = new System.Drawing.Size(100, 22);
            this.textBox_CellID.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "CellID:";
            // 
            // textBox_AngleZ
            // 
            this.textBox_AngleZ.Location = new System.Drawing.Point(255, 199);
            this.textBox_AngleZ.Name = "textBox_AngleZ";
            this.textBox_AngleZ.Size = new System.Drawing.Size(100, 22);
            this.textBox_AngleZ.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(191, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Angle Z:";
            // 
            // textBox_AngleY
            // 
            this.textBox_AngleY.Location = new System.Drawing.Point(255, 171);
            this.textBox_AngleY.Name = "textBox_AngleY";
            this.textBox_AngleY.Size = new System.Drawing.Size(100, 22);
            this.textBox_AngleY.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(191, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Angle Y:";
            // 
            // textBox_PosZ
            // 
            this.textBox_PosZ.Location = new System.Drawing.Point(85, 171);
            this.textBox_PosZ.Name = "textBox_PosZ";
            this.textBox_PosZ.Size = new System.Drawing.Size(100, 22);
            this.textBox_PosZ.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "Position Z:";
            // 
            // textBox_AngleX
            // 
            this.textBox_AngleX.Location = new System.Drawing.Point(255, 143);
            this.textBox_AngleX.Name = "textBox_AngleX";
            this.textBox_AngleX.Size = new System.Drawing.Size(100, 22);
            this.textBox_AngleX.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(191, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Angle X:";
            // 
            // textBox_PosY
            // 
            this.textBox_PosY.Location = new System.Drawing.Point(85, 143);
            this.textBox_PosY.Name = "textBox_PosY";
            this.textBox_PosY.Size = new System.Drawing.Size(100, 22);
            this.textBox_PosY.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Position Y:";
            // 
            // textBox_AngleW
            // 
            this.textBox_AngleW.Location = new System.Drawing.Point(255, 115);
            this.textBox_AngleW.Name = "textBox_AngleW";
            this.textBox_AngleW.Size = new System.Drawing.Size(100, 22);
            this.textBox_AngleW.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Angle W:";
            // 
            // textBox_PosX
            // 
            this.textBox_PosX.Location = new System.Drawing.Point(85, 115);
            this.textBox_PosX.Name = "textBox_PosX";
            this.textBox_PosX.Size = new System.Drawing.Size(100, 22);
            this.textBox_PosX.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Position X:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ACACWorldSpawnManipulator";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuFolder.ResumeLayout(false);
            this.contextMenuWeenie.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuFolder;
        private System.Windows.Forms.ContextMenuStrip contextMenuWeenie;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reorderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renumberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_WCID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_CellID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_AngleZ;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_AngleY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_PosZ;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_AngleX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_PosY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_AngleW;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_PosX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_ParseLoc;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button_ParseLocClear;
        private System.Windows.Forms.Button button_ParseLoc;
        private System.Windows.Forms.Button button_DeleteWeenie;
        private System.Windows.Forms.Button button_ClearWeenie;
        private System.Windows.Forms.Button button_UpdateWeenie;
        private System.Windows.Forms.TextBox textBox_WeenieID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button_AddNew;
        private System.Windows.Forms.RichTextBox rtfOutput;
    }
}

