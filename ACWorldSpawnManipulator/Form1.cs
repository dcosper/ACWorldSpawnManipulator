﻿using ACWorldSpawnManipulator.JsonClasses.LandBlocks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACACWorldSpawnManipulator
{
    public partial class Form1 : Form
    {
        WorldSpawn currentWorldSpawn = null;

        // going to try and fix weenie id errors
        List<long> allWeenieIDs = new List<long>();

        public Form1()
        {
            InitializeComponent();
            treeView1.AfterExpand += TreeView1_AfterExpand;
            treeView1.AfterCollapse += TreeView1_AfterCollapse;
            treeView1.MouseClick += TreeView1_MouseClick;
            treeView1.AfterSelect += TreeView1_AfterSelect;

            WriteLine("Log started.", Color.Green);
        }
        #region Event handlers
        /// <summary>
        /// Updates using up/down arrows on keyboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                Weenie w = treeView1.SelectedNode.Tag as Weenie;

                if (w != null)
                    LoadWeenie(w);
            }
        }
        /// <summary>
        /// Changes folder icon to collapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            // force icon to collapse 
            e.Node.SelectedImageIndex = 0;
        }
        /// <summary>
        /// Changes folder icon to expand
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            // force select of node
            treeView1.SelectedNode = e.Node;
            // force icon to expanded
            e.Node.SelectedImageIndex = 1;
        }
        /// <summary>
        /// Updates on mouse click, left selects, right opens menu 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_MouseClick(object sender, MouseEventArgs e)
        {
            TreeViewHitTestInfo info = treeView1.HitTest(treeView1.PointToClient(Cursor.Position));

            if (e.Button == MouseButtons.Right)
            {
                if (info != null)
                {
                    if (info.Node.SelectedImageIndex <= 1)
                    {
                        // show folder right click menu
                        contextMenuFolder.Show(treeView1.PointToScreen(e.Location));
                    }
                    else
                    {
                        // Show weenie right click menu
                        contextMenuWeenie.Show(treeView1.PointToScreen(e.Location));
                    }
                }
            }
            if (e.Button == MouseButtons.Left)
            {
                if (info != null)
                {
                    treeView1.SelectedNode = info.Node;

                    if (info.Node.SelectedImageIndex == 2) // weenie
                    {
                        Weenie w = info.Node.Tag as Weenie;

                        if(w != null)
                        {
                            LoadWeenie(w);
                        }
                    }
                }
            }
        }
        #endregion
        #region Buttons Menus
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "WorldSpawn|worldspawns.json";

            if (open.ShowDialog() == DialogResult.OK)
            {
                string data = string.Empty;

                try
                {
                    data = File.ReadAllText(open.FileName);

                    currentWorldSpawn = Newtonsoft.Json.JsonConvert.DeserializeObject<WorldSpawn>(data);

                    // fix this list
                    // Force sort (List sort wont work here)
                    IEnumerable<LandBlockSpawn> landBlockSpawns =
                        from landblock in currentWorldSpawn.LandBlockSpawns
                        orderby landblock.Key
                        select landblock;

                    currentWorldSpawn.LandBlockSpawns = landBlockSpawns.ToList();

                    PopulateTreeView();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, string.Format("Bad WorldSpawn File {0}", open.FileName));
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(currentWorldSpawn == null)
            {
                MessageBox.Show("Please load or create a new Worldspawn file before saving.");
                return;
            }

            SaveFileDialog save = new SaveFileDialog() {
                Filter = "WorldSpawn|worldspawn.json",
                Title = "Save as Worldspawn file."
            };

            if(save.ShowDialog() == DialogResult.OK)
            {
                string data = string.Empty;

                try
                {
                    data = Newtonsoft.Json.JsonConvert.SerializeObject(currentWorldSpawn);

                    File.WriteAllText(save.FileName ,data);
                }
                catch (Exception ex) { WriteLine(ex.Message, Color.Red); }
            }
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog()
            {
                Filter = "Landspawn Json Dumps|*.json",
                Title = "Locate a Landspawn json dump"
            };

            if(open.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    LandBlockSpawn lbs = Newtonsoft.Json.JsonConvert.DeserializeObject<LandBlockSpawn>(File.ReadAllText(open.FileName));

                    if(lbs.Key == -1)
                    {
                        MessageBox.Show("Invalid Landblock file!");
                        return;
                    }

                    if (currentWorldSpawn == null) // if we are null make a new one
                        currentWorldSpawn = new WorldSpawn();

                    if (currentWorldSpawn.GetLandBlockByKey(lbs.Key) != null)
                    {
                        // we exist

                        DialogResult warning = MessageBox.Show(string.Format("Landblock {0} exists, this will replace this landblock and not merge the contents, are you sure you want to do this ?", lbs.Key), "Duplicate Landblock detected!!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                        if (warning == DialogResult.Yes)
                        {
                            currentWorldSpawn.RemoveLandBlockByKey(lbs.Key);
                            currentWorldSpawn.LandBlockSpawns.Add(lbs);
                            PopulateTreeView();
                            MessageBox.Show(string.Format("Sucessfully Replaced Landblock ID {0}.", lbs.Key));
                        }
                    }
                    else
                    {
                        currentWorldSpawn.LandBlockSpawns.Add(lbs);
                        PopulateTreeView();
                        MessageBox.Show(string.Format("Sucessfully Imported Landblock ID {0}.", lbs.Key));
                    }
                }
                catch (Exception ex) { WriteLine(ex.Message, Color.Red); }
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentWorldSpawn == null)
            {
                MessageBox.Show("Please load or create a new Worldspawn file before exporting.");
                return;
            }

            DialogResult question = MessageBox.Show("This will dump each individual Landblock json to a directory, are you sure you wish to do this ?", "Dump Worldspawn to Landblock Jsons.", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if(question == DialogResult.Yes)
            {
                FolderBrowserDialog folder = new FolderBrowserDialog();
                folder.Description = "Select folder to dump all landblocks into, will overwrite any duplicates.";

                if(folder.ShowDialog() == DialogResult.OK)
                {
                    foreach(LandBlockSpawn lbs in currentWorldSpawn.LandBlockSpawns)
                    {
                        string data = string.Empty;

                        try
                        {
                            string filePath = Path.Combine(folder.SelectedPath, string.Format("{0}.json", lbs.Key));
                            data = Newtonsoft.Json.JsonConvert.SerializeObject(lbs);
                            File.WriteAllText(filePath, data);
                        }
                        catch (Exception ex) { WriteLine(ex.Message, Color.Red); }
                    }
                }
            }

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void button_ParseLoc_Click(object sender, EventArgs e)
        {
            // Your location is: 0x0007012D [59.991066 -3.761159 0.005000] 0.011754 0.000000 0.000000 -0.999931
            string loc = textBox_ParseLoc.Text + "\r";
            // crappy but works, bite me
            Regex regex = new Regex(": (.*?) \\[(.*?) (.*?) (.*?)\\] (.*?) (.*?) (.*?) (.*?)\r");

            if (regex.IsMatch(loc))
            {
                float posX = 0f, posY = 0f, posZ = 0f, angleW = 0f, angleX = 0f, angleY = 0f, angleZ = 0f;
                long cellID = 0;

                if (!float.TryParse(regex.Match(loc).Groups[2].Value, out posX) ||
                    !float.TryParse(regex.Match(loc).Groups[3].Value, out posY) ||
                    !float.TryParse(regex.Match(loc).Groups[4].Value, out posZ) ||
                    !float.TryParse(regex.Match(loc).Groups[5].Value, out angleW) ||
                    !float.TryParse(regex.Match(loc).Groups[6].Value, out angleX) ||
                    !float.TryParse(regex.Match(loc).Groups[7].Value, out angleY) ||
                    !float.TryParse(regex.Match(loc).Groups[8].Value, out angleZ))
                {
                    MessageBox.Show("Unable to parse location string!");
                }

                try
                {
                    cellID = long.Parse(regex.Match(loc).Groups[1].Value.Substring(2), System.Globalization.NumberStyles.HexNumber);
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }

                textBox_PosX.Text = posX.ToString();
                textBox_PosY.Text = posY.ToString();
                textBox_PosZ.Text = posZ.ToString();
                textBox_AngleW.Text = angleW.ToString();
                textBox_AngleX.Text = angleX.ToString();
                textBox_AngleY.Text = angleY.ToString();
                textBox_AngleZ.Text = angleZ.ToString();
                textBox_CellID.Text = cellID.ToString();
            }
        }

        private void button_ParseLocClear_Click(object sender, EventArgs e)
        {
            textBox_ParseLoc.Text = string.Empty;
        }

        private void button_ClearWeenie_Click(object sender, EventArgs e)
        {
            textBox_PosX.Text = string.Empty;
            textBox_PosY.Text = string.Empty;
            textBox_PosZ.Text = string.Empty;
            textBox_AngleW.Text = string.Empty;
            textBox_AngleX.Text = string.Empty;
            textBox_AngleY.Text = string.Empty;
            textBox_AngleZ.Text = string.Empty;
            textBox_CellID.Text = string.Empty;
            textBox_WCID.Text = string.Empty;
            textBox_WeenieID.Text = string.Empty;
            // unselect
            treeView1.SelectedNode = null;
        }

        private void button_UpdateWeenie_Click(object sender, EventArgs e)
        {
            // need to check weenie id here
            Weenie w = treeView1.SelectedNode.Tag as Weenie;

            if(w != null)
            {

            }
        }

        private void button_DeleteWeenie_Click(object sender, EventArgs e)
        {
            // get slected weenie
            if(treeView1.SelectedNode != null)
            {
                // get Parent
                LandBlockSpawn lbs = treeView1.SelectedNode.Parent.Tag as LandBlockSpawn;
                // get weenie
                Weenie w = treeView1.SelectedNode.Tag as Weenie;


                if (lbs != null)
                {
                    if (w != null)
                    {
                        try
                        {
                            long ID = w.ID, key = lbs.Key;

                            lbs.Value.RemoveWeenieByID(w.ID);
                            
                            LandBlockSpawn replace = new LandBlockSpawn(lbs);

                            treeView1.SelectedNode.Remove();

                            button_ClearWeenie_Click(sender, e);

                             CreateNode(replace);

                            WriteLine(string.Format("Removed Weenie {0} from Landblock {1}", ID, key));
                        }
                        catch (Exception ex) { WriteLine(ex.Message, Color.Red); }
                    }
                }
            }
        }
        #endregion
        #region Misc Functions
        private TreeNode CreateNode(LandBlockSpawn spawn)
        {
            TreeNode result = new TreeNode(spawn.Key.ToString(), 0, 0);
            result.Tag = spawn;

            foreach (Weenie w in spawn.Value.Weenies)
            {
                TreeNode child = new TreeNode(w.ID.ToString(), 2, 2);
                child.Tag = w;
                result.Nodes.Add(child);
            }
            return result;
        }
        private void AddWeenieFromData()
        {
            bool created = true;
            float x = -1, y = -1, z = -1, aw = -1, ax = -1, ay = -1, az = -1;
            long cellID = -1, weenieID = -1, wcid = -1;

            if(!float.TryParse(textBox_PosX.Text, out x) ||
                !float.TryParse(textBox_PosY.Text, out y) ||
                !float.TryParse(textBox_PosZ.Text, out z) ||
                !float.TryParse(textBox_AngleW.Text, out aw) ||
                !float.TryParse(textBox_AngleX.Text, out ax) ||
                !float.TryParse(textBox_AngleY.Text, out ay) ||
                !float.TryParse(textBox_AngleZ.Text, out az))
            {
                created = false;
            }

            if(!long.TryParse(textBox_CellID.Text, out cellID) ||
                !long.TryParse(textBox_WCID.Text, out wcid))
            {
                created = false;
            }

            
            if(!created)
            {
                MessageBox.Show("Unable to create new weenie!");
                return;
            }
            // need to find out where its gonna go, and get next weenie id from that

            LandBlockSpawn lbs = currentWorldSpawn.GetLandBlockByKey(GetLandblockIDFromCell(cellID));

            if(lbs == null)
            {

                // we need to create it
                lbs = new LandBlockSpawn();
                lbs.Key = GetLandblockIDFromCell(cellID);

                // weenieid may be blank
                if (!long.TryParse(textBox_WeenieID.Text, out weenieID))
                {
                    weenieID = lbs.Value.GetNextWeenieID();
                }
                
                lbs.Value.Weenies.Add(new Weenie(weenieID, new Position(x, y, z, aw, ax, ay,az, cellID), wcid));

                currentWorldSpawn.LandBlockSpawns.Add(lbs);

                CreateNode(lbs);
            }
            else
            {
                // weenieid may be blank
                if (!long.TryParse(textBox_WeenieID.Text, out weenieID))
                {
                    weenieID = lbs.Value.GetNextWeenieID();
                }

                lbs.Value.Weenies.Add(new Weenie(weenieID, new Position(x, y, z, aw, ax, ay, az, cellID), wcid));
                PopulateTreeView();
            }
            
        }
        /// <summary>
        /// Loads weenie info into editing section
        /// </summary>
        /// <param name="w"></param>
        private void LoadWeenie(Weenie w)
        {
            textBox_PosX.Text = w.Position.Frame.Origin.X.ToString();
            textBox_PosY.Text = w.Position.Frame.Origin.Y.ToString();
            textBox_PosZ.Text = w.Position.Frame.Origin.Z.ToString();
            textBox_AngleW.Text = w.Position.Frame.Angle.W.ToString();
            textBox_AngleX.Text = w.Position.Frame.Angle.X.ToString();
            textBox_AngleY.Text = w.Position.Frame.Angle.Y.ToString();
            textBox_AngleZ.Text = w.Position.Frame.Angle.Z.ToString();
            textBox_CellID.Text = w.Position.ObjCellID.ToString();
            textBox_WeenieID.Text = w.ID.ToString();
            textBox_WCID.Text = w.WCID.ToString();
        }
        /// <summary>
        /// Threaded populate treeview
        /// </summary>
        private void PopulateTreeView()
        {
            Thread t = new Thread(() =>
            {
                //if (treeView1.InvokeRequired)
               // {
                    treeView1.Invoke((MethodInvoker)
                        delegate
                        {
                            InternalPopulateTreeView();
                        });
                //}
               // else InternalPopulateTreeView();
            });
            t.TrySetApartmentState(ApartmentState.MTA);
            t.Start();
        }
        /// <summary>
        /// Direct access to treeview contents
        /// </summary>
        private void InternalPopulateTreeView()
        {
            treeView1.Nodes.Clear();

            foreach (LandBlockSpawn lbs in currentWorldSpawn.LandBlockSpawns)
            {
                treeView1.Nodes.Add(CreateNode(lbs));

                // now try weenie list, does nothing atm, just lets us know 
                // we have weenies with same id on different landblocks
                // I know that needs fixed, but need more info about what and why

                foreach (Weenie w in lbs.Value.Weenies)
                {
                    try
                    {
                        allWeenieIDs.Add(w.ID);
                    }
                    catch (Exception ex) { WriteLine(ex.Message, Color.Red); }
                }

            }

            WriteLine(string.Format("Loaded {0} weenie ids", allWeenieIDs.Count));

            var query = allWeenieIDs.GroupBy(x => x)
                .Where(g => g.Count() > 1)
                .Select(y => y.Key)
                .ToList();

            WriteLine(string.Format("{0} duplicate weenie ids found", query.Count));
        }
        /// <summary>
        /// Returns the Landblock Key this cellis belongs too
        /// </summary>
        /// <param name="cellID"></param>
        /// <returns></returns>
        private long GetLandblockIDFromCell(long cellID)
        {
            return cellID & 4294901760;
        }
        #endregion
        #region Write To Rtf
        /// <summary>
        /// Writes to log output, this is not really needed i was just tierd of messageboxes
        /// </summary>
        /// <param name="message"></param>
        private void WriteLine(string message)
        {
            WriteLine(message, Color.Black);
        }
        private void WriteLine(string message, Color color)
        {
            if(rtfOutput.InvokeRequired)
            {
                rtfOutput.Invoke((MethodInvoker)
                    delegate
                    {
                        rtfOutput.ForeColor = color;
                        rtfOutput.Text = rtfOutput.Text + message + "\n";
                    });
            }
            else
            {
                rtfOutput.ForeColor = color;
                rtfOutput.Text = rtfOutput.Text + message + "\n";
            }
        }
        #endregion
        #region Context Menus
        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // weenie delete
            button_DeleteWeenie_Click(sender, e);
        }

        private void exportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LandBlockSpawn lbs = treeView1.SelectedNode.Tag as LandBlockSpawn;

            if(lbs != null)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "LandblockSpawn|*.json";
                save.Title = "Export Landblock";

                // Force file name, user can change
                save.FileName = string.Format("{0}.json", lbs.Key);

                if(save.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        string data = Newtonsoft.Json.JsonConvert.SerializeObject(lbs);
                        File.WriteAllText(save.FileName, data);
                    }
                    catch (Exception ex) { WriteLine(ex.Message, Color.Red); }
                }
            }
        }
        private void reorderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // landblock weenie renumber
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LandBlockSpawn lbs = treeView1.SelectedNode.Tag as LandBlockSpawn;

            if(lbs != null)
            {
                currentWorldSpawn.RemoveLandBlockByKey(lbs.Key);

                treeView1.SelectedNode.Remove();
            }
        }

        private void renumberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // weenie renumber
        }
        #endregion
    }
}
